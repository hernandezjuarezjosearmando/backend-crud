-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 29-06-2024 a las 22:39:47
-- Versión del servidor: 10.4.25-MariaDB
-- Versión de PHP: 8.0.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `crud`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `products`
--

INSERT INTO `products` (`id`, `name`, `price`) VALUES
(1, 'Unbranded Wooden Ball', '791.93'),
(2, 'Refined Fresh Towels', '438.20'),
(3, 'Handcrafted Soft Car', '662.95'),
(4, 'Handmade Steel Ball', '324.63'),
(5, 'Rustic Concrete Bike', '718.12'),
(6, 'Handcrafted Wooden Shoes', '508.34'),
(7, 'Gorgeous Rubber Salad', '16.67'),
(8, 'Intelligent Concrete Hat', '260.81'),
(9, 'Gorgeous Metal Table', '415.64'),
(10, 'Handcrafted Frozen Sausages', '284.08'),
(11, 'Practical Rubber Cheese', '870.17'),
(12, 'Handcrafted Frozen Salad', '668.76'),
(13, 'Fantastic Soft Keyboard', '365.90'),
(14, 'Sleek Concrete Pizza', '251.80'),
(15, 'Small Granite Sausages', '134.72'),
(16, 'Intelligent Cotton Mouse', '785.56'),
(17, 'Tasty Wooden Keyboard', '572.14'),
(18, 'Unbranded Cotton Tuna', '70.05'),
(19, 'Handcrafted Concrete Pants', '182.62'),
(20, 'Incredible Metal Gloves', '363.86'),
(21, 'Intelligent Fresh Car', '649.68'),
(22, 'Practical Cotton Chicken', '163.74'),
(23, 'Small Plastic Pants', '433.02'),
(24, 'Refined Rubber Keyboard', '292.31'),
(25, 'Generic Plastic Tuna', '905.80'),
(26, 'Awesome Plastic Mouse', '122.59'),
(27, 'Tasty Wooden Shirt', '452.25'),
(28, 'Handcrafted Granite Chicken', '42.94'),
(29, 'Gorgeous Metal Tuna', '585.10'),
(30, 'Ergonomic Fresh Chair', '943.48'),
(31, 'Small Frozen Sausages', '343.85'),
(32, 'Sleek Wooden Table', '518.24'),
(33, 'Small Metal Gloves', '666.70'),
(34, 'Tasty Concrete Keyboard', '13.48'),
(35, 'Generic Plastic Soap', '862.68'),
(36, 'Handcrafted Fresh Tuna', '131.13'),
(37, 'Unbranded Plastic Mouse', '764.74'),
(38, 'Azúcar', '276.58'),
(39, 'Cafe', '52.42'),
(40, 'Coca cola', '965.90'),
(41, 'Salsa', '856.98'),
(42, 'Tortillas', '419.57'),
(43, 'Huevo', '67.73'),
(44, 'Vasos', '644.59'),
(45, 'Agua', '862.75'),
(46, 'Shampoo', '295.24'),
(47, 'Detergente', '311.89'),
(48, 'Jabón', '199.04'),
(49, 'Aceite', '526.41'),
(50, 'Papel', '971.16');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email`, `password`, `role`) VALUES
(1, 'Armando', 'mandos12', 'armando@gmail.com', '$2b$10$cGvOst4aWC5KQlDri52c8uocG/P8wCyTr3pz6NxuHOIUgp7r8k6LC', 'ADMIN');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
