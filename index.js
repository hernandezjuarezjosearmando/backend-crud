const express = require('express');
const cors = require('cors');
const app = express();
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const db = require('./db');
const authenticateToken = require('./authMiddleware');
const port = 3000;

app.use(cors());

app.use(bodyParser.json());

const SECRET_KEY = '~fU54jKwcce_*[}u6p5y|];z~T';

app.post('/api/auth/signin', (req, res) => {
  const { usernameOrEmail, password } = req.body;
  const query = 'SELECT * FROM users WHERE username = ? OR email = ?';
  db.query(query, [usernameOrEmail, usernameOrEmail], async (err, results) => {
    if (err) {
      return res.status(500).json({ error: err.message });
    }
    if (results.length === 0) {
      return res.status(401).json({ error: 'Nombre de usuario o contraseña incorrectos' });
    }

    const user = results[0];
    const isValidPassword = await bcrypt.compare(password, user.password);
    if (!isValidPassword) {
      return res.status(401).json({ error: 'Nombre de usuario o contraseña incorrectos' });
    }

    const token = jwt.sign({ id: user.id, role: user.role }, SECRET_KEY, { expiresIn: '1h' });
    res.json({
      accessToken: token,
      tokenType: 'Bearer',
      role: user.role
    });
  });
});

app.post('/api/auth/signup', async (req, res) => {
  const { name, username, email, password } = req.body;
  const role = 'user';

  try {
    const userExistsQuery = 'SELECT * FROM users WHERE username = ? OR email = ?';
    db.query(userExistsQuery, [username, email], async (err, results) => {
      if (err) {
        return res.status(500).json({ error: err.message });
      }
      if (results.length > 0) {
        return res.status(400).json({ error: 'Usuario o correo ya existen' });
      }

      const hashedPassword = await bcrypt.hash(password, 10);
      const query = 'INSERT INTO users (name, username, email, password, role) VALUES (?, ?, ?, ?, ?)';
      db.query(query, [name, username, email, hashedPassword, role], (err, result) => {
        if (err) {
          res.status(500).json({ error: err.message });
        } else {
          res.status(201).send("User registered successfully!.");
        }
      });
    });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

app.post('/api/auth/signup', (req, res) => {
    const { name, username, email, password } = req.body;
    if (name === 'Armando') {
        res.status(400).json({ timestamp: "2024-06-28", message: "Name is already exists!.", details: "uri=/api/auth/signup" });
        return;
    }

    if (username === 'mandos12') {
        res.status(400).json({ timestamp: "2024-06-28", message: "Username is already exists!.", details: "uri=/api/auth/signup" });
        return;
    }

    if (email === 'armandoexample.com') {
        res.status(400).json({ timestamp: "2024-06-28", message: "Email is already exists!.", details: "uri=/api/auth/signup" });
        return;
    }

    return res.status(201).json("User registered successfully!.");
});

app.get('/api/products', authenticateToken, (req, res) => {
  const { search } = req.query;

  let query = 'SELECT * FROM products';
  let params = [];

  if (search) {
    query += ' WHERE name LIKE ? OR price LIKE ?';
    params = [`%${search}%`, `%${search}%`];
  }

  query += ' ORDER BY id DESC';

  db.query(query, params, (err, results) => {
    if (err) {
      return res.status(500).json({ error: err.message });
    }
    res.send(results);
  });
});

app.post('/api/products', (req, res) => {
  const { name, price } = req.body;
  const query = 'INSERT INTO products (name, price) VALUES (?, ?)';
  db.query(query, [name, price], (err, result) => {
    if (err) {
      res.status(500).json({ error: err.message });
    } else {
      res.status(201).json({ id: result.insertId, name, price });
    }
  });
});

app.put('/api/products/:id', (req, res) => {
  const productId = req.params.id;
  const { name, price } = req.body;
  
  // Primero, verificar si el nombre ya existe en otro producto
  const checkQuery = 'SELECT COUNT(*) as count FROM products WHERE name = ? AND id <> ?';
  db.query(checkQuery, [name, productId], (checkErr, checkResult) => {
    if (checkErr) {
      res.status(500).json({ error: checkErr.message });
      return;
    }

    const existingCount = checkResult[0].count;
    if (existingCount > 0) {
      res.status(400).json({ error: 'Ya existe otro producto con el mismo nombre' });
      return;
    }

    // Si el nombre no existe en otro producto, proceder con la actualización
    const updateQuery = 'UPDATE products SET name = ?, price = ? WHERE id = ?';
    db.query(updateQuery, [name, price, productId], (updateErr, updateResult) => {
      if (updateErr) {
        res.status(500).json({ error: updateErr.message });
      } else {
        if (updateResult.affectedRows > 0) {
          res.status(200).json({ id: productId, name, price });
        } else {
          res.status(404).json({ error: 'Producto no encontrado' });
        }
      }
    });
  });
});

app.delete('/api/products/:id', (req, res) => {
  const productId = req.params.id;

  // Consulta SQL para eliminar el producto por su ID
  const query = 'DELETE FROM products WHERE id = ?';
  db.query(query, [productId], (err, result) => {
    if (err) {
      res.status(500).json({ error: err.message });
    } else {
      if (result.affectedRows > 0) {
        res.status(200).json({ message: `Producto con ID ${productId} eliminado correctamente` });
      } else {
        res.status(404).json({ error: 'Producto no encontrado' });
      }
    }
  });
});

app.listen(port, () => {
  console.log(`Server running on http://localhost:${port}`);
});
